﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class AccelerometerInput : NetworkBehaviour
{
    public static NetworkInstanceId nid;
    private void Start()
    {
        nid = this.netId;
        Input.gyro.enabled = true;
    }
    void Update()
    {
        //transform.Translate(Input.acceleration.x, 0, 0);
        if (isLocalPlayer)
        {
            transform.position = new Vector3(Input.acceleration.x, Input.acceleration.y, Input.acceleration.z);
            GyroModifyCamera();
        }
    }

    // The Gyroscope is right-handed.  Unity is left handed.
    // Make the necessary change to the camera.
    void GyroModifyCamera()
    {
        transform.rotation = GyroToUnity(Input.gyro.attitude);


    }

    private static Quaternion GyroToUnity(Quaternion q)
    {
        return new Quaternion(q.x, q.y, -q.z, -q.w);
    }

    protected void OnGUI()
    {
        GUI.skin.label.fontSize = Screen.width / 40;

        GUILayout.Label("Orientation: " + Screen.orientation);
        GUILayout.Label("input.gyro.enabled: " + Input.gyro.enabled);
        GUILayout.Label("input.gyro.attitude: " + Input.gyro.attitude);
        GUILayout.Label("input.gyro.gravity: " + Input.gyro.gravity);
        GUILayout.Label("input.gyro.rotationRate: " + Input.gyro.rotationRate);
        GUILayout.Label("input.gyro.rotationRateUnbiased: " + Input.gyro.rotationRateUnbiased);
        GUILayout.Label("input.acceleration: " + Input.acceleration);
        GUILayout.Label("iphone width/font: " + Screen.width + " : " + GUI.skin.label.fontSize);
    }
}