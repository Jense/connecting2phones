﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClientBehaviour : MonoBehaviour {

    private Client client;
    private float sendTimer = 0.0f;

    public int port = 9999;
    public string ip = "192.168.11.4";

    // The id we use to identify our messages and register the handler
    public short messageID = 1000;

    public InputField ipField;
    public Button connectButton;
    Text buttonText;
    
    // Use this for initialization
    void Start () {
        Input.gyro.enabled = true;
        buttonText = connectButton.transform.Find("Text").GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        if (client != null && client.isConnected)
        {
            sendTimer += Time.deltaTime;
            
            if (sendTimer >= Input.gyro.updateInterval)
            {
                this.client.sendMessage(messageID, createMessage());
                sendTimer = 0.0f;
            }
        }

        if (client != null && client.isConnected)
        {
            buttonText.text = "Disconnect";
        }

        else if (client != null && !client.isConnected)
        {
            buttonText.text = "Connect";
        }
        if (client != null)
            Debug.Log(this.client.isConnected);
    }

    public void handleButton()
    {
        if (this.client == null || !this.client.isConnected)
        {
            ip = ipField.text;
            Debug.Log("Connect Button: " + ip);
            this.client = new Client(ip, port);
        }
        else if (!this.client.isConnected)
        {
            ip = ipField.text;
            this.client.connect(ip);
            Debug.Log("Client reconnect");
        }
        else if (this.client.isConnected)
        {
            this.client.disconnect();
            Debug.Log("Client disconnect");
        }
    }

    AndroidMessage createMessage ()
    {
        AndroidMessage messageContainer = new AndroidMessage();
        messageContainer.acc = Input.acceleration;
        messageContainer.attitude = Input.gyro.attitude;
        messageContainer.gravity = Input.gyro.gravity;
        messageContainer.rotationRate = Input.gyro.rotationRate;
        messageContainer.rotationRateUnbiased = Input.gyro.rotationRateUnbiased;
        messageContainer.enabled = Input.gyro.enabled;

        return messageContainer;
    }

    protected void OnGUI()
    {
        GUI.skin.label.fontSize = Screen.width / 20;

        GUILayout.Label("Orientation: " + Screen.orientation);
        GUILayout.Label("input.gyro.enabled: " + Input.gyro.enabled);
        GUILayout.Label("input.gyro.attitude: " + Input.gyro.attitude);
        GUILayout.Label("input.gyro.gravity: " + Input.gyro.gravity);
        GUILayout.Label("input.gyro.rotationRate: " + Input.gyro.rotationRate);
        GUILayout.Label("input.gyro.rotationRateUnbiased: " + Input.gyro.rotationRateUnbiased);
        GUILayout.Label("input.acceleration: " + Input.acceleration);
        GUILayout.Label("iphone width/font: " + Screen.width + " : " + GUI.skin.label.fontSize);
    }
}
